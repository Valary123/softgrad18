SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `industries` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `industries` (`id`, `name`) VALUES
(1, 'Travels and tourism'),
(2, 'Project management software'),
(3, 'Government purchases'),
(4, 'Education'),
(5, 'Marketing'),
(6, 'Charity'),
(7, 'Events organization'),
(8, 'HR'),
(9, 'Trading'),
(10, 'Visual constructor'),
(11, 'Insurance'),
(12, 'Trading'),
(13, 'Cars'),
(14, 'Culinary'),
(15, 'Social networks'),
(16, 'Online bookshop'),
(17, 'Marketplace'),
(18, 'Voice Assistant'),
(19, 'BI Systems'),
(20, 'Medicine'),
(21, 'Entertaiment');

CREATE TABLE `industries_relations` (
  `project_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `industries_relations` (`project_id`, `industry_id`) VALUES
(1, 1),
(2, 4),
(3, 3),
(4, 2),
(5, 6),
(6, 10),
(7, 12),
(8, 11),
(9, 5),
(10, 5),
(11, 7),
(12, 4),
(13, 14),
(14, 2),
(15, 8),
(16, 9),
(17, 8),
(18, 11),
(19, 13);

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `original` text NOT NULL,
  `thumbnail` text NOT NULL,
  `alt` text NOT NULL,
  `pos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `photos` (`id`, `original`, `thumbnail`, `alt`, `pos`) VALUES
(1, './public/photos/1.jpg', './public/photos/1_t.jpg', 'Team photo', 0),
(2, './public/photos/2.jpg', './public/photos/2_t.jpg', 'Team photo', 1),
(3, './public/photos/3.jpg', './public/photos/3_t.jpg', 'Team photo', 2),
(4, './public/photos/4.jpg', './public/photos/4_t.jpg', 'Team photo', 3),
(5, './public/photos/5.jpg', './public/photos/5_t.jpg', 'Team photo', 4),
(6, './public/photos/6.jpg', './public/photos/6_t.jpg', 'Team photo', 5),
(7, './public/photos/7.jpg', './public/photos/7_t.jpg', 'Team photo', 6);

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `pos` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `full_description` text NOT NULL,
  `img` text NOT NULL,
  `img_thumbnail` text NOT NULL,
  `team` text NOT NULL,
  `country` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `projects` (`id`, `pos`, `name`, `description`, `full_description`, `img`, `img_thumbnail`, `team`, `country`) VALUES
(1, 0, 'Civitours', 'Online platform for booking activities, day trips and guided tours in the most visited tourist destinations of Europe and North America.', 'Civitours allows to search and visit many historical and resort places in Europe and North America. Tourists can book their tours in advance and manage their travels through private profiles. On the admin side we developed a whole system of tours, cities and countries creation and management, as well as advanced reports system.', 'public/projects/1.jpg', 'public/projects/1.jpg', '5', 'Spain'),
(2, 1, 'Innovative Language Learning', 'System of the foreign languages learning through lessons, podcasts, forums. That covers 34 languages from all continents.', 'This constantly growing project with 5 million unique visitors and has several key functionalities: MyTeacher — web chat with a teacher; Flashcards — learning words through flash cards; Word Bank — user\'s dictionary; Vocabulary Lists — thematic word lists; Time tracking spent on site.', 'public/projects/2.jpg', 'public/projects/2.jpg', '9', 'Japan/USA'),
(3, 2, 'Phortress', 'CRM for government purchases.', 'CRM is integrated with external services for getting government purchases details. CRM automates process of getting prices details from specific distributor by following ways: fax, printed document, email, interactive web-form. Or prices details can be taken from previous prices requests or from procurement history. Pricing details are combines together with stats and represents in graphical view and reports. User uses it for analyzing and making offer to government or to another company. CRM tracks status of offer and uses rich set of tools for further offer processing. Creation of purchase order, acknowledge contract to distributor and government/another company, track received and shipping status of items etc.', 'public/projects/3.jpg', 'public/projects/3.jpg', '5', 'USA'),
(4, 3, 'ProjectSputnik', 'Project management system developed for inner purposes that allows to manage projects and teams, tracking time and create different reports based on parameters of projects flow and effectiveness of employees.', 'System allows to create project’s environment, invite members, create tasks and assign them to specific responsible persons. Every employee has his own list of the tasks, status of working on it, time spent and time estimated. Based on these and some other parameters it is possible to calculate parameters of efficiency. As well as generate different reports: Work duration for client, Time analysis, Project efficiency, Employee timelog, Employees effectiveness, Ratio of estimated hours to actual time, Online invoices - they will include list of the tasks per each employee, time spent for each of the tasks and all together, time range.', 'public/projects/4.jpg', 'public/projects/4.jpg', '3', 'Russia'),
(5, 4, 'SOMS', 'SOMS helps charities, the Saving Company (SC), and sale persons (entrepreneurs) to earn funds as they are involved in the “saving process”.', 'This project helps charities, the Saving Company (SC), and sale persons (entrepreneurs) to earn funds as they are involved in the “saving process”. The SC presents a website which has a home page. This home page contains some related information and a charity list which are the separate sites freely accessible by a user friendly URL. The SC is also responsible for promoting the charities’ shopping sites spreading the shopping site links wherever possible.', 'public/projects/5.jpg', 'public/projects/5.jpg', '3', 'USA'),
(6, 5, 'Amaze Designer', 'Amaze Designer allows to create heat maps for Amaze Analytics by residing map’s components on the canvas. Working process is similar as in other visual builders like WPF components builder in Visual Studio, Window Builder for Eclipse and Sencha Architect.', 'User can drag and drop components from the toolbar to the canvas, and configure them. Components are resided on the canvas using different layouts. User can configure selected components using property and style editors. The application generates XML config by component tree on the canvas. By generated XML config Amaze Analytics can build fully-functional heat map.', 'public/projects/6.jpg', 'public/projects/6.jpg', '2', 'USA'),
(7, 6, 'FxVerify', 'Fxverify is the only place for authentic ratings and reviews for trading by fully verified real customers.', 'Fxverify is the only place for authentic ratings and reviews for trading by fully verified real customers. Project used technologies provide blazing-fast response and great SEO based on SSR. Every review is automatically translated into 20 languages and verified by “fingerprints” technology.', 'public/projects/7.png', 'public/projects/7.png', '6', 'USA'),
(8, 7, 'Simon Agency', 'Online service that allows customers get quick access to the products of Insurance companies in more than 30 industries through entire USA and each state.', 'Simon Agency TriQuote TM system provides access to Online, Instant Issue and or traditional “Shop it Around” quoting capabilities from 30+ insurance companies. At the click of a simple button, secure coverage from your mobile, desktop and tablet devices for over 175 classifications and markets.', 'public/projects/8.png', 'public/projects/8.png', '7', 'USA'),
(10, 8, 'IntoSales СRМ', 'CRM Integrated more than 100 merchant gateways for different countries and national currencies: US, Canada, Germany, Great Britain, Brazil, Argentina.', 'Advanced tools for payments processing: online and offline payment processing, recurring payments processing, authorization/void/settlements/refunds, advanced merchant gateways load balancing etc. APIs for integration of external systems. PCI DSS and PA DSS certificated. High-loaded and big-data system,  Over 100 different reports based on collected stats: partial/customers activity, sales/returns stats, failed sales stats, CSRs activity, fulfillments.', 'public/projects/9.jpg', 'public/projects/9.jpg', '5', 'USA'),
(11, 9, 'Make Events', 'Client visits the public resource, registers, and now can build order which may include venue and artist programs.', 'Client visits the public resource, registers, and now can build order which may include venue and artist programs. Selecting a venue, reading detailed information, price, etc, before booking it.  Venue and artists may provide discounts and additions which are visible on the calendar in week and month perspectives. The system also has administrative tools for providers (venue and artist). They can manage orders, see payments, add programs and required/provided equipment, manage their additions, discount, inaccessible dates using calendar. The calendar is built from scratch on EmberJS as well it allows user select any number of hour/date cells in week/month modes respectively. It is used in public and private parts of the system.', 'public/projects/10.jpg', 'public/projects/10.jpg', '3', 'Russia'),
(12, 10, 'Megaschool', 'Educational service for learning different subjects, similar to Coursera.', 'An online educational service which is organized in different courses, groupis of students. Each group has its own teacher and curator. Students communicate with their teacher and each other, have access to lessons material and take exams through the system.', 'public/projects/11.jpg', 'public/projects/11.jpg', '3', 'Israel'),
(13, 11, 'Go4eat', 'Online service of organizing culinary events in Israel.', 'Service for creation culinary events, inviting people or searching for specific events and online booking. Admin part has advanced reports system.', 'public/projects/12.jpg', 'public/projects/12.jpg', '4', 'Israel'),
(14, 12, 'OfficeTimeSheets', 'Project or Job Based Timesheet Managment, Project and/or Job Cost Management, Detail and Summary Level Reports and Analysis, Two-Way QuickBooks, Microsoft Project Integration.', 'Web-based commercial employee time tracking and management timesheets software application designed for the specialized needs of project-driven organizations to track time and expenses for accurate accounting and job/project costing purposes, and real-time reporting and billing.', 'public/projects/13.jpg', 'public/projects/13.jpg', '6', 'USA'),
(15, 13, 'HR planning', 'Rich frontend application for HR planning.', 'HR planning in an organization which can be as single one or can have hierarchical structure. Customer create organization structure, add employees, and start drawing timebars. There are also open/release/close period wizards wish freezes the created schedules as part of workflow.', 'public/projects/14.jpg', 'public/projects/14.jpg', '2', 'USA'),
(16, 14, 'CTA Insights', 'Rich frontend application.', 'Contains many interactive charts, allow users to observe reach trading statistics, search relevant statistics with smart filters and full-text search, build individual portfolios and so on.', 'public/projects/15.jpg', 'public/projects/15.jpg', '2', 'USA'),
(17, 15, 'HR Expert', 'Human relations online service that allows to get access to different types of HR information.', 'HR Expert is Australia’s most comprehensive online human resource information collection. It is your easy-touse one stop solution for professional people management processes, procedures and documentation. HR Expert adds clarity to every aspect of people management, providing people managers with professionally written, fully customisable templates that cover the entire range of Human Resource needs of your business.', 'public/projects/16.jpg', 'public/projects/16.jpg', '3', 'Australia'),
(18, 16, 'Egislife', 'Protect life by providing personal, social & financial incentives that affect positive change within the insurance industry.', 'EgisLife provides custom rate insurance for users and businesses. We combine data science & technology to serve the community.', 'public/projects/17.jpg', 'public/projects/17.jpg', '3', 'USA'),
(19, 17, 'Cars Sales', 'Cars selling platform.', 'Cars selling platform: workshop reviews, buy&sell, events, car reviews.', 'public/projects/18.jpg', 'public/projects/18.jpg', '3', 'New Zealand'),
(20, 18, 'OfficeCalendar', 'OfficeCalendar is a computer software program that is an alternative to Microsoft Exchange Server.', 'OfficeCalendar is a computer software program that is an alternative to Microsoft Exchange Server. OfficeCalendar allows its\' users to share Microsoft Outlook calendar, contact, and task information with other Microsoft Outlook users on a computer network. OfficeCalendar makes Outlook calendar sharing and group scheduling easy and affordable with its\' interactive group calendars because it does not require Microsoft\'s Exchange Server, which is considered too expensive and too complicated for small businesses.', 'public/projects/20.jpg', 'public/projects/20.jpg', '4', 'USA');

CREATE TABLE `project_types` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project_types` (`id`, `name`) VALUES
(1, 'Website Development'),
(2, 'Online Data Driven Applications'),
(3, 'Web Based Business and Workflow Applications'),
(4, 'Front-end Design & Development'),
(5, 'Complex Backend Systems with API Integrations'),
(6, 'Software product development');

CREATE TABLE `project_types_relations` (
  `project_id` int(11) NOT NULL,
  `project_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project_types_relations` (`project_id`, `project_type_id`) VALUES
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(6, 2),
(6, 4),
(6, 5),
(7, 1),
(7, 2),
(7, 4),
(7, 5),
(8, 1),
(8, 2),
(8, 4),
(8, 5),
(9, 2),
(9, 4),
(9, 5),
(9, 6),
(10, 2),
(10, 4),
(10, 5),
(10, 6),
(11, 2),
(11, 3),
(11, 4),
(11, 5),
(12, 2),
(12, 3),
(12, 4),
(12, 5),
(13, 1),
(13, 2),
(13, 3),
(13, 4),
(13, 5),
(14, 6),
(14, 2),
(14, 3),
(14, 4),
(14, 5),
(15, 6),
(15, 2),
(15, 3),
(15, 4),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(18, 2),
(18, 3),
(18, 4),
(18, 5),
(19, 2),
(19, 3),
(19, 4),
(20, 6),
(20, 2);

CREATE TABLE `technologies` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `icon` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `technologies` (`id`, `name`, `icon`) VALUES
(1, 'HTML5', 'devicon-html5-plain'),
(2, 'Javascript', 'devicon-javascript-plain'),
(3, 'CSS3', 'devicon-css3-plain'),
(4, 'SASS', 'devicon-sass-original'),
(5, 'React', 'devicon-react-original'),
(6, 'Angular', 'devicon-angularjs-plain'),
(7, 'Vue', 'devicon-vuejs-plain'),
(8, 'Babel', 'devicon-babel-plain'),
(9, 'jQuery', 'devicon-jquery-plain'),
(10, 'NodeJS', 'devicon-nodejs-plain'),
(11, 'PHP', 'devicon-php-plain'),
(12, 'C#', 'devicon-csharp-plain'),
(13, '.NET', 'devicon-dot-net-plain'),
(14, 'Python', 'devicon-python-plain'),
(15, 'PostgreSQL', 'devicon-postgresql-plain'),
(16, 'MongoDB', 'devicon-mongodb-plain'),
(17, 'MySQL', 'devicon-mysql-plain'),
(18, 'AWS', 'devicon-amazonwebservices-original'),
(19, 'Ember', 'devicon-javascript-plain'),
(20, 'C++', 'devicon-cplusplus-plain'),
(21, 'BackboneJS', 'devicon-backbonejs-plain');

CREATE TABLE `technologies_relations` (
  `project_id` int(11) NOT NULL,
  `technology_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `technologies_relations` (`project_id`, `technology_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 6),
(1, 11),
(1, 17),
(2, 1),
(2, 3),
(2, 4),
(2, 2),
(2, 11),
(2, 17),
(2, 18),
(3, 1),
(3, 3),
(3, 4),
(3, 2),
(3, 13),
(3, 17),
(4, 1),
(4, 3),
(4, 2),
(4, 13),
(4, 17),
(5, 1),
(5, 3),
(5, 2),
(5, 13),
(5, 5),
(5, 10),
(5, 15),
(6, 1),
(6, 3),
(6, 2),
(6, 9),
(6, 8),
(6, 5),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(7, 8),
(7, 10),
(7, 18),
(7, 16),
(8, 1),
(8, 2),
(8, 3),
(8, 11),
(8, 17),
(9, 1),
(9, 2),
(9, 3),
(9, 11),
(9, 17),
(10, 1),
(10, 2),
(10, 3),
(10, 11),
(10, 17),
(11, 1),
(11, 2),
(11, 3),
(11, 5),
(11, 15),
(12, 1),
(12, 2),
(12, 3),
(12, 11),
(12, 17),
(13, 1),
(13, 2),
(13, 3),
(13, 11),
(13, 17),
(14, 1),
(14, 2),
(14, 3),
(14, 9),
(14, 13),
(14, 17),
(15, 1),
(15, 2),
(15, 3),
(15, 19),
(16, 1),
(16, 2),
(16, 3),
(16, 9),
(16, 6),
(16, 10),
(17, 1),
(17, 3),
(17, 9),
(17, 2),
(17, 11),
(17, 17),
(18, 1),
(18, 3),
(18, 9),
(18, 2),
(18, 11),
(18, 17),
(19, 1),
(19, 3),
(19, 2),
(19, 5),
(20, 12),
(20, 13);

CREATE TABLE vacancies (
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    slug VARCHAR(255) NOT NULL,
    pos INT(11) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    full_description TEXT NOT NULL
 )ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO vacancies (id, slug, pos, name, description, full_description) VALUES
(1, 'Full-Stack-developer', 0, 'Full Stack developer', 'Ищем опытного Full Stack разработчика для развития одного из самых популярных на рынке сервисов для изучения иностранных языков (iOS, Android, Web).', '<p>Ищем опытного Full Stack&nbsp;разработчика для развития одного из самых популярных на рынке сервисов для изучения иностранных языков (iOS, Android, Web).&nbsp;Но также готовы рассмотреть кандидатуры специалистов со знанием только back-end и только front-end.</p>
<p>Наша задача сделать изучение иностранных языков удобным и доступным для пользователей всех стран. Поэтому любой в команде может предложить свою идею и посоветовать как можно сделать лучше, ну и, конечно, помочь.</p>
<p><b>Наши преимущества:</b></p>
<ul>
<li><i class="fa fa-check" aria-hidden="true"></i> Интересный проект и нестандартные технические задачи</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Гибкий график для оптимального баланса работы и личной жизни</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Возможность влиять на развитие команды и продукта</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Возможность повышения квалификации за счет компании</li>
</ul>
<p><b>Требования:</b></p>
<ul>
<li><i class="fa fa-check" aria-hidden="true"></i> Хорошие знания PHP&nbsp;5/7 (в том числе: умение отлаживать код, читать логи; понимание работы HTTP-протокола, механизма сессий; правильная фильтрация входных и экранирование выходных данных)</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Понимание ООП (классы, наследование, интерфейсы, трейты и пр.)</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Понимание принципов MVC</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Понимание основ безопасности веб-приложений</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Знание верстки на HTML, Bootstrap</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Знание HTML, CSS и Javascript</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Опыт работы с системами управления версиями (git)</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Знание SQL (типы данных, индексы, джоины, производительность запросов и пр.)</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Английский язык</li>
</ul>
<p><b>Будет плюсом:</b></p>
<ul>
<li><i class="fa fa-check" aria-hidden="true"></i> Знание фреймворков Laravel, Yii, Symfony</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Опыт работы с mongodb, sphinx</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Опыт работы с Javascript библиотеками: react, vue, backbonejs</li>
</ul>
<p><b>Что мы предлагаем:</b></p>
<ul>
<li><i class="fa fa-check" aria-hidden="true"></i> Достойная и своевременная заработная плата</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Официальное трудоустройство и оформление по ТК</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Возможность расти и развиваться, прокачивая свои навыки и знания (мы всегда рады успеху наших сотрудников)</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Удобный график работы</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Расположение офиса в центре города</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Дружный коллектив</li>
<li><i class="fa fa-check" aria-hidden="true"></i> Рост заработной платы по мере развития </li>
</ul>');


ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `project_types`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `technologies`
  ADD PRIMARY KEY (`id`);

ALTER TABLE vacancies
    ADD UNIQUE INDEX (slug);

ALTER TABLE `industries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

ALTER TABLE `project_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

ALTER TABLE `technologies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
