## Установка ##

### Требования ###

```
PHP 5.6 и выше
MySQL 5 и выше (нужно импортировать содержимое softgrad.sql)
Node.js и Yarn (для сборки фронта)
Angular CLI (yarn add -g @angular/cli)
```

### .env ###

Пример конфигурации .env есть в .env.example

```
APP_URL=http://localhost    <---- Начальный путь для роутинга (необходим для нормальной работы Puppeteer)
ROUTER_BASE=/dist           <---- Путь для роутинга, нужно указать если работа идет в подпапке
BACKEND_BASE=/dist/         <---- Путь к папке backend
DB_HOSTNAME=localhost       <---- Адрес для подключения к базе данных
DB_USER=root                <---- Логин пользователя БД
DB_PASSWORD=                <---- Пароль пользователя БД
DB_DATABASE=softgrad        <---- Название базы данных
```

Если запускаете проект в докере, то `ROUTER_BASE=/`

## Разработка

### Устанавливаем пакеты

```bash
yarn
cd src/angular && yarn
```

### Сборка 

Начальная сборка: `yarn run build-angular && yarn dev`.

Для dev сборки `yarn dev`.

Для разработки с watch запустить `yarn dev --watch`.

Далее с зайти на проект с помощью веб-сервера аля http://localhost/softgrad18/dist/

При изменении статических файлов (изображения, видео, шрифты) необходимо запускать `yarn run copy-assets`.

При изменении ангуляр компонента запускать `yarn run build-angular`.

## Прод

Для сборки прода нужен продовский .env и далее: ` yarn run prod `

### Сборка для обкатки на демо сайте

```bash
cp .env .env_local && \
cp .env_prod_demo .env
yarn run prod && \
mv .env_local .env
```
Затем заливаем содержимое папки `dist` на сервер в `demos/sg-site`. Тестим: https://softgrad.com/demos/sg-site/

// fixme что-то пока не работает :(

### Сборка для настоящего прода

```bash
cp .env .env_local && \
cp .env_prod .env
yarn run prod && \
mv .env_local .env
```
Заливаем содержимое папки `dist` на сервер в `public_html`

### Сборка

После этого необходимо настроить веб сервер на папку dist.

## Докер

Mysql сервер скорее всего у вас установлен локально, поэтому создайте БД ``softgrad`` и пользователя `softgrad`, который сможет коннектиться к этой БД с любого хоста (%). Затем запустите `sudo ip addr show docker0` и установите IP в `DB_HOSTNAME` в `.env`. 

```bash
docker-compose build
docker-compose up
```

Открываем http://localhost:8081/ в браузере.
