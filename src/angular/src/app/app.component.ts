import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {RecaptchaComponent} from 'ng-recaptcha';


class DataForm {
    constructor(
        public name = '',
        public email = '',
        public phone = '',
        public subject = '',
        public message = '',
        public token = '',
        public referrer = '',
        public attachment = ''
    ) {
    }
}

interface PostResult {
    type: string;
    message: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})

export class AppComponent implements OnInit, AfterViewInit {
    private readonly FORM_KEY = 'formData';

    submitForm: FormGroup;
    postResult: PostResult = {type: '', message: ''};
    showNotify = false;
    checkFields = false;
    inProgress = false;
    formData: FormData;
    formElement: HTMLFormElement;
    @ViewChild('captchaRef') recaptchaRef: RecaptchaComponent;
    private data = new DataForm();

    /**
     * constructor
     * @param {HttpClient} http
     */
    constructor(private http: HttpClient) {
    }

    /**
     *
     */
    ngOnInit() {
        const stored = localStorage.getItem(this.FORM_KEY);
        if (null !== stored) {
            try {
                this.data = JSON.parse(stored);
            } catch (e) {
                console.error(e);
            }
        }

        const referrer = localStorage.getItem('referrerData');

        if (null !== referrer) {
            try {
                const parsed = JSON.parse(referrer);

                if (parsed.expires && new Date().getTime() >= parsed.expires) {
                    localStorage.removeItem('referrerData');
                } else {
                    if (!this.data.subject) { this.data.subject = parsed.subject; }
                    this.data.referrer = parsed.referrer;
                }
            } catch (e) {
                console.log(e);
            }
        }
        this.createForm();
        this.submitForm.valueChanges.subscribe(value => {
            Object.assign(this.data, value);
            localStorage.setItem(this.FORM_KEY, JSON.stringify(value)); // Save changed data
        });
    }

    /**
     * Initialize recaptcha after view
     */
    ngAfterViewInit() {
        this.recaptchaRef.resolved.subscribe((captcha) => {
            if (null === captcha) {
                // captcha has been reset
                return;
            }

            this.formData.append('token', captcha);
            // const headers = new HttpHeaders ({'X-Requested-With': 'xmlhttprequest', 'Content-Type':'application/json'});
            this.http.post<PostResult>('./backend/contact.php', this.formData)
                .toPromise()
                .then(res => {
                    this.postResult = res;
                    this.showNotify = true;
                    this.inProgress = false;
                    this.recaptchaRef.reset();
                    this.clearForm();
                })
                .catch(error => {
                    this.postResult.type = 'danger';
                    this.postResult.message = error.message;
                    this.showNotify = true;
                    this.inProgress = false;
                    this.recaptchaRef.reset();
                });
        });
    }

    /**
     * Create form class
     */
    private createForm() {
        this.submitForm = new FormGroup({
            name: new FormControl(this.data.name, [
                Validators.required,
                Validators.maxLength(30)
            ]),
            email: new FormControl(this.data.email, [
                Validators.required, Validators.email]),
            phone: new FormControl(this.data.phone, [
                this.regexValidator(/^(\+?[\d\-]+)*$/)]),
            subject: new FormControl(this.data.subject, {
                validators: [Validators.maxLength(100)],
                updateOn: 'change'
            }),
            message: new FormControl(this.data.message, {
                validators: [Validators.required, Validators.maxLength(1000)],
                updateOn: 'change'
            })
        }, {updateOn: 'blur'});
    }

    /**
     * Clears form and storage
     */
    private clearForm() {
        localStorage.removeItem(this.FORM_KEY);
        this.checkFields = false;
        this.submitForm.reset();
        this.formElement.reset();
    }

    /**
     * Custom regexp validation function. Value should pass the regexp
     * @param {RegExp} nameRe
     * @returns {ValidatorFn}
     */
    private regexValidator(nameRe: RegExp): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const value = control.value || '';
            const passed = nameRe.test(value);
            return passed ? null : {'regexp': {value: value}};
        };
    }

    /**
     * closes notification dialog
     */
    closeNotify() {
        this.showNotify = false;
    }

    /**
     * Submit data to server
     */
    onSubmit(event) {
        this.checkFields = true;
        if (this.submitForm.invalid) {
            return;
        }
        this.formElement = event.target;
        this.formData = new FormData(event.target);
        this.inProgress = true;
        this.showNotify = false;
        this.recaptchaRef.execute();
    }

    /** form fields getters */
    get name() {
        return this.submitForm.get('name');
    }

    get email() {
        return this.submitForm.get('email');
    }

    get phone() {
        return this.submitForm.get('phone');
    }

    get subject() {
        return this.submitForm.get('subject');
    }

    get message() {
        return this.submitForm.get('message');
    }

    get referrer() {
        return this.data.referrer;
    }
}
