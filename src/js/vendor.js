import './vendor/bootstrap.min';

import './vendor/modernizr-custom.js';
import './vendor/scrollIt.min.js';

import './vendor/flowplayer/flowplayer-3.2.13.min.js';

import 'ammap3/ammap/ammap.js';
import 'ammap3/ammap/plugins/export/export.min.js';
import 'ammap3/ammap/themes/light.js';
import 'ammap3/ammap/maps/js/worldLow.js';
