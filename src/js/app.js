import VModal from 'vue-js-modal';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Index from './views/Index.vue';
import Vacancies from './views/Vacancies.vue';
import Projects from './views/Projects.vue';
import Error404 from './views/Error404.vue';
import Layout from './views/Layout.vue';

Vue.use(VModal, { dialog: true });
Vue.use(VueRouter);
Vue.config.ignoredElements = ['app-root'];

const router = new VueRouter({
  base: process.env.ROUTER_BASE,
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Index,
    },
    {
      path: '/vacancies',
      component: Vacancies,
    },
    {
      path: '/projects/filter/types/:types/technologies/:technologies/industries/:industries',
      name: 'projects-filtered',
      component: Projects,
    },
    {
      path: '/projects',
      name: 'projects',
      component: Projects,
    },
    {
      path: '*',
      component: Error404,
    },
  ],
});

/* Scroll fix on route change */
router.beforeEach((to, from, next) => {
  window.scrollTo(0, 0);
  next();
});

document.getElementById('app').innerHTML = '<layout></layout>';

new Vue({
  el: '#app',
  components: { Layout },
  router
});
