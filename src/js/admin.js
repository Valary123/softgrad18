import Vue from 'vue';
import Editor from '@tinymce/tinymce-vue';
import ImageInput from './components/ImageInput.vue';
import MultiSelect from './components/MultiSelect.vue';
import './vendor/bootstrap.min';

new Vue({
  el: '#app',
  components: {
    editor: Editor,
    imageinput: ImageInput,
    multiselect: MultiSelect
  },
  data() {
    return {
      login: '',
      password: '',
      isAuthorized: false,
      photos: [],
      projects: [],
      vacancies: [],
      photoForm: {
        original: '',
        alt: '',
      },
      projectForm: {
        name: '',
        description: '',
        full_description: '',
        img: '',
        team: '',
        country: '',
        technologies: [],
        project_types: [],
        industries: [],
      },
      vacancyForm: {
        name: '',
        description: '',
        full_description: '',
      },
      projectsMeta: [],
      loginError: '',
    };
  },
  computed: {
    navbarClasses() {
      return ['navbar-header', 'not-home-navbar'];
    },
    navbarMenuClasses() {
      return ['nav', 'navbar-nav', 'navbar-right', 'not-home-menu'];
    }
  },
  created() {
    this.checkAuthorization();
  },
  methods: {
    /**
     * Makes authorization check request
     */
    checkAuthorization() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, { action: 'checkAuthorization' })
        .then(response => {
          this.isAuthorized = response.auth;
          this.getPhotos();
          this.getProjectsMeta();
          this.getProjects();
          this.getVacancies();
        });
    },
    makeRequest: (url, data) => fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
    })
      .then(response => response.json()),
    /**
     * Makes authorization request
     */
    authorize() {
      this.loginError = '';

      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'authorize',
        login: this.login,
        password: this.password,
      })
        .then(response => {
          if (response.error) {
            this.loginError = response.error;
            return;
          }

          this.isAuthorized = response.auth;
          this.getPhotos();
        });
    },
    /**
     * Fetches photo-list.json from the server
     */
    getPhotos() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/gallery.php`, { action: 'getPhotos' })
        .then(response => {
          const photos = response.sort(this.sortByPosition);
          this.photos = photos;
        });
    },
    getProjectsMeta() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/projects.php`, { action: 'getProjectsMeta' })
        .then(meta => {
          this.projectsMeta = meta;
        });
    },
    /**
     * Fetches projects from the server
     */
    getProjects() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/projects.php`, { action: 'getProjects' })
        .then(projects => {
          projects = projects.sort(this.sortByPosition);
          this.projects = projects;
        });
    },
    getVacancies() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/vacancies.php`, { action: 'getVacancies', allFields: 1 })
        .then(response => {
          this.vacancies = response;
        });
    },
    moveUp(array, key) {
      if (key === 0) return;
      const movingObject = array.splice(key, 1)[0];
      array.splice(key - 1, 0, movingObject);
    },
    moveDown(array, key) {
      if (array.length - 1 === key) return;
      const movingObject = array.splice(key, 1)[0];
      array.splice(key + 1, 0, movingObject);
    },
    moveProjectUp(key) {
      this.moveUp(this.projects, key);
      this.recalculatePositions(this.projects);
      this.updateProjectsPositions();
    },
    moveProjectDown(key) {
      this.moveDown(this.projects, key);
      this.recalculatePositions(this.projects);
      this.updateProjectsPositions();
    },
    moveVacancyUp(key) {
      this.moveUp(this.vacancies, key);
      this.recalculatePositions(this.vacancies);
      this.updateVacanciesPositions();
    },
    moveVacancyDown(key) {
      this.moveDown(this.vacancies, key);
      this.recalculatePositions(this.vacancies);
      this.updateVacanciesPositions();
    },
    updateVacanciesPositions() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'updateVacanciesPositions',
        vacancies: this.vacancies.map(({ id, pos }) => ({ id, pos })),
      });
    },
    updateProjectsPositions() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'updateProjectsPositions',
        projects: this.projects,
      });
    },
    updatePhotosPositions() {
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'updatePhotosPositions',
        photos: this.photos,
      });
    },
    removeVacancy(key) {
      const removedVacancy = this.vacancies.splice(key, 1)[0];
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'removeVacancy',
        id: removedVacancy.id,
      });
    },
    removeProject(key) {
      const removedProject = this.projects.splice(key, 1)[0];
      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'removeProject',
        id: removedProject.id,
      });
    },
    editVacancy(key) {
      this.vacancyForm = this.vacancies[key];
      window.scrollTo(0, 0);
    },
    editProject(key) {
      this.projectForm = this.projects[key];
      window.scrollTo(0, 0);
    },
    editPhoto(key) {
      this.photoForm = this.photos[key];
      window.scrollTo(0, 0);
    },
    movePhoto(key, direction) {
      direction === 'up' ? this.moveUp(this.photos, key) : this.moveDown(this.photos, key);
      this.recalculatePositions(this.photos);
      this.updatePhotosPositions();
    },
    movePhotoUp(key) {
      this.movePhoto(key, 'up');
    },
    movePhotoDown(key) {
      this.movePhoto(key, 'down');
    },
    removePhoto(key) {
      const removedPhoto = this.photos.splice(key, 1)[0];

      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'removePhoto',
        id: removedPhoto.id,
      });
    },
    addPhoto() {
      this.photoForm.pos = this.photoForm.id ? this.photoForm.pos : 0;

      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'addPhoto',
        photo: this.photoForm,
      })
        .then(response => {
          if (response.error) return;

          if (!this.photoForm.id) {
            this.photos.unshift(response);
            this.recalculatePositions(this.photos);
            this.updatePhotosPositions();
          } else {
            const itemIndex = this.photos.findIndex(photo => photo.id === this.photoForm.id);
            this.photos[itemIndex] = response;
          }

          this.resetPhotoForm();
        });
    },
    addProject() {
      this.projectForm.pos = this.projectForm.id ? this.projectForm.pos : 0;

      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'addProject',
        project: this.projectForm,
      })
        .then(response => {
          if (response.error) return;

          if (!this.projectForm.id) {
            this.projects.unshift(response);
            this.recalculatePositions(this.projects);
            this.updateProjectsPositions();
          } else {
            const itemIndex = this.projects.findIndex(project => project.id === this.projectForm.id);
            this.projects[itemIndex] = response;
          }

          this.resetProjectForm();
        });
    },
    addVacancy() {
      this.vacancyForm.pos = this.vacancyForm.id ? this.vacancyForm.pos : 0;

      this.makeRequest(`${process.env.BACKEND_BASE}backend/admin.php`, {
        action: 'addVacancy',
        vacancy: this.vacancyForm,
      })
        .then(response => {
          if (response.error) {
            alert(response.error);
            return;
          }

          if (!this.vacancyForm.id) {
            this.vacancies.unshift(response);
            this.recalculatePositions(this.vacancies);
            this.updateVacanciesPositions();
          } else {
            const itemIndex = this.vacancies.findIndex(item => item.id === this.vacancyForm.id);
            this.vacancies[itemIndex] = response;
          }

          this.resetVacancyForm();
        });
    },
    /**
     * Clears form
     */
    resetVacancyForm() {
      const fields = ['id', 'pos', 'name', 'description', 'full_description'];
      for (const item of fields) this.vacancyForm[item] = '';
      this.$refs.vacancyForm.reset();
    },
    resetProjectForm() {
      const fields = ['id', 'pos', 'name', 'description', 'full_description', 'img', 'team', 'country'];
      const arrays = ['technologies', 'project_types', 'industries'];

      for (const item of fields) this.projectForm[item] = '';
      for (const item of arrays) this.projectForm[item] = [];

      this.$refs.projectForm.reset();
    },
    resetPhotoForm() {
      const fields = ['id', 'pos', 'alt', 'original'];
      for (const item of fields) this.photoForm[item] = '';
      this.$refs.photoForm.reset();
    },
    recalculatePositions: array => array.forEach((item, key) => item.pos = key),
    sortByPosition: (a, b) => a.pos - b.pos,
    sortByPositionDesc: (a, b) => b.pos - a.pos,
  },
});
