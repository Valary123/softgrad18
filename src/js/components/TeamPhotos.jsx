import React from 'react';
import ImageGallery from 'react-image-gallery';

export default class TeamPhotos extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      images: [],
    };
  }

  componentDidMount() {
    fetch(`${process.env.BACKEND_BASE}backend/gallery.php`, {
      method: 'POST',
      body: JSON.stringify({
        action: 'getPhotos',
      }),
    })
      .then(response => response.json())
      .then(response => {
        /* Throw out empty entries and sort remaining */
        const images = response.filter(image => !(!image.original || !image.thumbnail))
          .sort((a, b) => a.pos - b.pos)
          .map(image => ({
            id: image.id,
            pos: image.pos,
            original: image.original,
            originalAlt: image.alt,
            thumbnail: image.thumbnail,
            thumbnailAlt: image.alt,
          }));

        this.setState({ images });
      });
  }

  render() {
    const { images } = this.state;

    return (
      <ImageGallery items={images} />
    );
  }
}
