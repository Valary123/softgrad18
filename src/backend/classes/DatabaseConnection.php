<?php

class DatabaseConnection
{
    private $connection;

    /**
     * DatabaseConnection constructor.
     */
    public function __construct()
    {
        $this->connection = new mysqli(
            getenv("DB_HOSTNAME"),
            getenv("DB_USER"),
            getenv("DB_PASSWORD"),
            getenv("DB_DATABASE")
        );

        $this->connection->set_charset("utf8");
    }

    /**
     * MySQLi instance getter
     * @return mysqli
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Executes query and returns assoc array when finished
     * @param $query
     * @return mixed
     */
    public function assocQuery($query)
    {
        if ($result = $this->connection->query($query)) {
            $rows = array();
            while ($row = $result->fetch_assoc()) {
                $rows[] = $row;
            }
            return $rows;
        }

        return [];
    }

    /**
     * Makes an select query and returns associative array
     * @param string $table
     * @param array $params additional params like "fields", "where", "order"
     * @return mixed
     */
    public function select($table, $params = [])
    {
        $where = $order = "";
        if (!empty($params["fields"])) {
            $fields = $params["fields"];
        } else {
            $fields = "*";
        }
        if (!empty($params["where"])) {
            $where = "WHERE " . $params["where"];
        }
        if (!empty($params["order"])) {
            $order = "ORDER BY " . $params["order"];
        }
        return $this->assocQuery("SELECT $fields FROM $table $where $order");
    }

    /**
     * Makes delete query
     * @param $table
     * @param $id
     * @return bool|mysqli_result
     */
    public function delete($table, $id)
    {
        return $this->connection->query("DELETE FROM $table WHERE id = $id");
    }

    /**
     * As name says
     * @param array|string|int $value
     * @return array
     */
    public function surroundWithQuotes($value)
    {
        if (is_array($value)) {
            return array_map(function ($item) {
                return "'{$this->connection->real_escape_string($item)}'";
            }, $value);
        } else {
            return "'{$this->connection->real_escape_string($value)}'";
        }

    }

    /**
     * Makes insert query
     * @param $table
     * @param $items
     * @return bool|mysqli_result
     */
    public function insert($table, $items)
    {
        $keys = implode(", ", array_keys($items));
        $values = implode(", ", $this->surroundWithQuotes(array_values($items)));

        return $this->connection->query("INSERT INTO $table ($keys) VALUES ($values)");
    }

    /**
     * Makes update query
     * @param $table
     * @param $id
     * @param $updates
     * @return bool|mysqli_result
     */
    public function update($table, $id, $updates)
    {
        array_walk($updates, function (&$value, $key) {
            $value = "$key = {$this->surroundWithQuotes($value)}";
        });

        $updates = implode(", ", $updates);
        return $this->connection->query("UPDATE $table SET $updates WHERE id = $id");
    }
}
