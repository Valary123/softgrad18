<?php

$MAX_FILE_SIZE = 10485760; // Maximum attachment size (equals to 10mb)

require 'classes/PHPMailer/Exception.php';
require 'classes/PHPMailer/PHPMailer.php';
require 'classes/PHPMailer/SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;

// Recaptcha check
if (!validateToken($_POST['token'])) {
    jsonResponse(array('type' => 'danger', 'message' => 'Captcha is not valid'));
    die();
}

// Mail compile
$mail = new PHPMailer(true);
$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'mail.softgrad.com';                 // Specify main and backup server
$mail->Port = 587;                                    // Set the SMTP port
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'info@softgrad.com';                // SMTP username
$mail->Password = 'P=.BbsFE7yZ+';                  // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

try {
    $mail->setFrom("info@softgrad.com", "Softgrad Feedback");
    $mail->addAddress("info@softgrad.com");
    $mail->addAddress("ed@softgrad.com");
    $mail->Subject = 'New message from contact form';

    $body = [];

    $attachment = $_FILES["attachment"];
    if (!$attachment["error"]) {
        if ($attachment["size"] > $MAX_FILE_SIZE) {
            jsonResponse([
                'type' => "danger",
                "message" => "File is too big, max file size is 10 MBytes"
            ]);

            exit();
        }

        $mail->addAttachment($attachment["tmp_name"], $attachment["name"]);
    }


    $fields = array('name' => 'Name', 'subject' => 'Subject', 'email' => 'Email', 'message' => 'Message', 'phone' => 'Phone');
    array_push($body, "You have new message from contact form");
    array_push($body, "=============================\r\n");
    foreach ($_POST as $key => $value) if (isset($fields[$key])) array_push($body, "$fields[$key]: $value");

    $mail->Body = implode("\r\n", $body);
    $mail->send();

    jsonResponse([
        'type' => 'success',
        'message' => 'Contact form successfully submitted. Thank you, I will get back to you soon!'
    ]);
} catch (Exception $exp) {
    jsonResponse([
        "type" => "danger",
        "message" => 'There was an error while submitting the form. Please try again later',
        "exception" => $exp->errorMessage(),
        "errorInfo" => $mail->ErrorInfo
    ]);
}

/**
 * Validate user token from recaptcha
 *
 * @param $token
 * @return mixed
 */
function validateToken($token)
{
    $result = makeRequest($token);
    if (!$result || !isset($result['success'])) {
        return false;
    }
    return $result['success'];
}


/**
 * Perform curl request to recaptcha service
 *
 * @param $token
 * @return mixed
 */
function makeRequest($token)
{

    $fields = "secret=6Lf4blEUAAAAAOHL_ws-4i7cDQHSqmGkXpvupCTX&response=$token";

    $post = curl_init();

    curl_setopt($post, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
    curl_setopt($post, CURLOPT_POST, 2);
    curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

    $result = json_decode(curl_exec($post), true);

    curl_close($post);
    return $result;
}

/**
 * Send JSON back to user
 *
 * @param $response
 */
function jsonResponse($response)
{
    header('Content-Type: application/json');
    echo json_encode($response);
}


