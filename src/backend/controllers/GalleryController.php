<?php

class GalleryController extends BaseController
{
    public function getPhotoAction($id = null)
    {
        $id = isset($id) ? $id : $this->request->id;
        return $this->dbConnection->getConnection()->query("SELECT * FROM photos WHERE id = $id")->fetch_assoc();
    }

    public function getPhotosAction()
    {
        return $this->dbConnection->select("photos");
    }
}
