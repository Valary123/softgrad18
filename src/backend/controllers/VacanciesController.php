<?php

class VacanciesController extends BaseController
{
    public function getVacanciesAction($allFields = null)
    {
        $allFields = isset($allFields) ? $allFields : $this->request->allFields;
        $params = [
            "order" => "pos",
        ];
        if (!$allFields) {
            $params['fields'] = "id, slug, name, description";
        }
        return $this->dbConnection->select("vacancies", $params);
    }

    public function getVacancyAction($id = null, $allFields = null)
    {
        $id = isset($id) ? $id : $this->request->id;
        if (!$id) return ["error" => "id is required"];
        $allFields = isset($allFields) ? $allFields : $this->request->allFields;
        $params = [
            "where" => "id = " . intval($id), // anti SQLi
        ];
        if (!$allFields) {
            $params['fields'] = "id, name, full_description";
        }
        $list = $this->dbConnection->select("vacancies", $params);
        return empty($list) ? null : $list[0];
    }

    public function getVacancyBySlugAction($slug = null, $allFields = null)
    {
        $slug = isset($slug) ? $slug : $this->request->slug;
        if (!$slug) return ["error" => "slug is required"];
        $allFields = isset($allFields) ? $allFields : $this->request->allFields;
        $params = [
            "where" => "slug = " . $this->dbConnection->surroundWithQuotes($slug),
        ];
        if (!$allFields) {
            $params['fields'] = "id, name, full_description";
        }
        $list = $this->dbConnection->select("vacancies", $params);
        return empty($list) ? null : $list[0];
    }
}
