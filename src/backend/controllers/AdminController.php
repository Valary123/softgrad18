<?php

require "ProjectsController.php";
require "GalleryController.php";
require "VacanciesController.php";

class AdminController extends BaseController
{
    /* Password is hashed by password_hash function. Replace before production */
    private $ADMIN_LOGIN = "admin";
    private $ADMIN_PASSWORD = '$2y$10$V/XIqFm8tMv.68BnXfukGeNFqesQs//0kpVhHgT0mCQ7QlzLhInw2';

    public function addProjectAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];

        $isUpdate = isset($this->request->project->id) && $this->request->project->id > 0;
        $fields = ["pos", "name", "description", "full_description", "team", "country"];

        $connectionInstance = $this->dbConnection->getConnection();
        if ($isUpdate) {
            $updates = [];

            foreach ($fields as $field) {
                if (!isset($this->request->project->$field)) continue;
                $updates[$field] = $this->request->project->$field;
            }

            $hasImage = !empty($this->request->project->img) ? strpos($this->request->project->img, "base64") !== false : false;

            if ($hasImage) {
                $savedImage = $this->saveImage($this->request->project->img, "public/projects");
                $updates["img"] = $savedImage["original"];
                $updates["img_thumbnail"] = $savedImage["thumbnail"];
            }

            $this->dbConnection->update("projects", $this->request->project->id, $updates);
        } else {
            $requiredFields = ["pos", "name", "img", "description", "full_description", "team", "country"];
            $validationResult = $this->validateExistance("project", $requiredFields);
            if ($validationResult["result"]) return ["errors" => $validationResult["errors"]];

            $savedImage = $this->saveImage($this->request->project->img, "public/projects");

            $projectData = $this->request->project;

            $data = [
                "pos" => $projectData->pos,
                "name" => $projectData->name,
                "description" => $projectData->description,
                "full_description" => $projectData->full_description,
                "img" => $savedImage["original"],
                "img_thumbnail" => $savedImage["thumbnail"],
                "team" => $projectData->team,
                "country" => $projectData->country
            ];

            $query = $this->dbConnection->insert("projects", $data);

            if (!$query) return ["error" => $connectionInstance->error];
        }

        $last_id = $isUpdate ? $this->request->project->id : $connectionInstance->insert_id;

        if ($isUpdate) {
            $tables = ["project_types_relations", "industries_relations", "technologies_relations"];
            foreach ($tables as $table) $connectionInstance->query("DELETE FROM $table WHERE project_id = $last_id");
        }

        /* Populate subfields */
        $subFields = ["technologies", "project_types", "industries"];
        foreach ($subFields as $subField) {
            $items = [];

            foreach ($this->request->project->$subField as $item) {
                array_push($items, $item->id);
            }

            $query = "INSERT INTO {$subField}_relations VALUES ";

            $itemsLength = sizeof($items);

            $items = array_map(function ($item) use ($last_id) {
                return "($last_id, $item)";
            }, $items);

            $query .= implode(", ", $items);

            $connectionInstance->query($query);
        }

        $projectsController = new ProjectsController();
        return $projectsController->getProjectAction($last_id);
    }

    private function convertNameToSlug($name)
    {
        return preg_replace('/[^a-z0-9\-_]+/i', '-', $name);
    }

    public function addVacancyAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];

        $isUpdate = isset($this->request->vacancy->id) && $this->request->vacancy->id > 0;
        $fields = ["pos", "name", "description", "full_description"];

        $connectionInstance = $this->dbConnection->getConnection();
        if ($isUpdate) {
            $updates = [];

            foreach ($fields as $field) {
                if (!isset($this->request->vacancy->$field)) continue;
                $updates[$field] = $this->request->vacancy->$field;
            }
            if (isset($updates['name'])) {
                $updates['slug'] = $this->convertNameToSlug($updates['name']);
            }

            $status = $this->dbConnection->update("vacancies", $this->request->vacancy->id, $updates);
            if (!$status) return ["error" => $connectionInstance->error];
        } else {
            $validationResult = $this->validateExistance("vacancy", $fields);
            if ($validationResult["result"]) return ["errors" => $validationResult["errors"]];

            $vacancyData = $this->request->vacancy;

            $data = [
                "pos" => $vacancyData->pos,
                "name" => $vacancyData->name,
                "slug" => $this->convertNameToSlug($vacancyData->name),
                "description" => $vacancyData->description,
                "full_description" => $vacancyData->full_description,
            ];

            $status = $this->dbConnection->insert("vacancies", $data);
            if (!$status) return ["error" => $connectionInstance->error];
        }

        $id = $isUpdate ? $this->request->vacancy->id : $connectionInstance->insert_id;

        $vacanciesController = new VacanciesController();
        return $vacanciesController->getVacancyAction($id, true);
    }

    public function addPhotoAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        $galleryController = new GalleryController();

        $isUpdate = isset($this->request->photo->id) && $this->request->photo->id > 0;

        if ($isUpdate) {
            $updates = [];

            $fields = ["pos", "alt"];
            foreach ($fields as $key => $field) {
                if (!isset($this->request->photo->$field)) continue;
                $updates[$field] = $this->request->photo->$field;
            }

            $hasImage = !empty($this->request->photo->original) ? strpos($this->request->photo->original, "base64") !== false : false;

            if ($hasImage) {
                $savedImage = $this->saveImage($this->request->photo->original, "public/projects");
                $updates["original"] = $savedImage["original"];
                $updates["thumbnail"] = $savedImage["thumbnail"];
            }

            $this->dbConnection->update("photos", $this->request->photo->id, $updates);
        } else {
            $savedImage = $this->saveImage($this->request->photo->original, "public/photos");
            $requiredFields = ["pos", "alt", "original"];
            $validationResult = $this->validateExistance("photo", $requiredFields);
            if ($validationResult["result"]) return ["errors" => $validationResult["errors"]];

            $data = [
                "original" => $savedImage["original"],
                "thumbnail" => $savedImage["thumbnail"],
                "alt" => $this->request->photo->alt,
                "pos" => $this->request->photo->pos
            ];

            $query = $this->dbConnection->insert("photos", $data);

            if (!$query) return ["error" => $this->dbConnection->getConnection()->error];
        }

        $last_id = $isUpdate ? $this->request->photo->id : $this->dbConnection->getConnection()->insert_id;
        return $galleryController->getPhotoAction($last_id);
    }

    private function validateExistance($array, $fields)
    {
        $errors = [];

        foreach ($fields as $key) {
            if (!isset($this->request->$array->$key)) {
                array_push($errors, "Field $key can't be empty");
                continue;
            }

            $field = $this->request->$array->$key;

            if (is_string($field) && empty($field)) {
                array_push($errors, "Field $key can't be empty");
                continue;
            }
        }

        if (!empty($errors)) return [
            "result" => true,
            "errors" => $errors
        ];

        return ["result" => false];
    }

    public function updateProjectsPositionsAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        $connectionInstance = $this->dbConnection->getConnection();
        $preparedStatement = $connectionInstance->prepare("UPDATE projects SET pos = ? WHERE id = ?");

        foreach ($this->request->projects as $project) {
            $preparedStatement->bind_param("ii", $project->pos, $project->id);
            $preparedStatement->execute();
        }

        return ["status" => "ok"];
    }

    public function updateVacanciesPositionsAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        $connectionInstance = $this->dbConnection->getConnection();
        $preparedStatement = $connectionInstance->prepare("UPDATE vacancies SET pos = ? WHERE id = ?");

        foreach ($this->request->vacancies as $vacancy) {
            $preparedStatement->bind_param("ii", $vacancy->pos, $vacancy->id);
            $preparedStatement->execute();
        }

        return ["status" => "ok"];
    }

    public function updatePhotosPositionsAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        $connectionInstance = $this->dbConnection->getConnection();
        $preparedStatement = $connectionInstance->prepare("UPDATE photos SET pos = ? WHERE id = ?");

        foreach ($this->request->photos as $photo) {
            $preparedStatement->bind_param("ii", $photo->pos, $photo->id);
            $preparedStatement->execute();
        }

        return [
            "status" => "ok"
        ];
    }

    public function removePhotoAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        if (!$this->dbConnection->delete("photos", $this->request->id)) return ["error" => $this->dbConnection->getConnection()->error];
        return ["status" => "ok"];
    }

    public function removeProjectAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        if (!$this->dbConnection->delete("projects", $this->request->id)) return ["error" => $this->dbConnection->getConnection()->error];
        return ["status" => "ok"];
    }

    public function removeVacancyAction()
    {
        if (!$this->authState()) return ["error" => "You are not authorized!"];
        if (!$this->dbConnection->delete("vacancies", $this->request->id)) return ["error" => $this->dbConnection->getConnection()->error];
        return ["status" => "ok"];
    }

    public function saveImage($imageString, $path, $originalWidth = 1000, $thumbnailWidth = 500)
    {
        $fileHash = md5(time());
        $srcImageHash = base64_decode(explode(",", $imageString, 2)[1]);
        $srcImage = imagecreatefromstring($srcImageHash);
        $srcHeight = imagesy($srcImage);
        $srcWidth = imagesx($srcImage);

        $originalRatio = $originalWidth / $srcWidth;
        $originalHeight = $srcHeight * $originalRatio;

        $thumbnailRatio = $thumbnailWidth / $srcWidth;
        $thumbnailHeight = $srcHeight * $thumbnailRatio;

        $original = imagecreatetruecolor($originalWidth, $originalHeight);
        $thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailHeight);

        imagecopyresampled($original, $srcImage, 0, 0, 0, 0, $originalWidth, $originalHeight, $srcWidth, $srcHeight);
        imagecopyresampled($thumbnail, $srcImage, 0, 0, 0, 0, $thumbnailWidth, $thumbnailHeight, $srcWidth, $srcHeight);

        $originalFilePath = "$path/$fileHash.jpg";
        $thumbnailFilePath = "$path/$fileHash" . "_t.jpg";

        imagejpeg($original, "../" . $originalFilePath, 95);
        imagejpeg($thumbnail, "../" . $thumbnailFilePath, 95);

        return [
            "original" => $originalFilePath,
            "thumbnail" => $thumbnailFilePath
        ];
    }

    /**
     * Action to authorize users
     * @return array
     */
    public function authorizeAction()
    {
        if (!$this->request->login) return ["error" => "You must specify login"];
        if (!$this->request->password) return ["error" => "You must specify password"];

        $loginMatches = $this->request->login == $this->ADMIN_LOGIN;
        $passwordMatches = password_verify($this->request->password, $this->ADMIN_PASSWORD);
        if ($loginMatches && $passwordMatches) {
            $_SESSION["auth"] = true;
            return ["auth" => $this->authState()];
        } else {
            return ["error" => "Your login or password does not match our records"];
        }
    }

    public function checkAuthorizationAction()
    {
        return ["auth" => $this->authState()];
    }

    /**
     * Returns current authorization state as boolean
     * @return bool
     */
    private function authState()
    {
        if ($_SESSION["auth"] == null) return false;
        return $_SESSION["auth"];
    }
}
