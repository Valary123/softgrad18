<?php

class ProjectsController extends BaseController
{
    public function getProjectsMetaAction()
    {
        $projectTypes = $this->dbConnection->select("project_types");
        $industries = $this->dbConnection->select("industries");
        $technologies = $this->dbConnection->select("technologies");

        return [
            "project_types" => $projectTypes,
            "industries" => $industries,
            "technologies" => $technologies
        ];
    }

    public function getProjectsAction()
    {
        // First we need to get all projects and their meta
        $projects = $this->dbConnection->select("projects");
        $projectsMeta = $this->getProjectsMetaAction();

        // Then we need to get all relations
        $relations = [];
        $relationKeys = ["project_types" => "project_type_id", "industries" => "industry_id", "technologies" => "technology_id"];
        foreach ($relationKeys as $key => $relation) $relations[$key] = $this->dbConnection->select("{$key}_relations");

        // Then we start populating projects with them
        // Pure magic btw
        foreach ($projects as &$project) {
            foreach ($relations as $key => $relationType) {
                $project[$key] = [];

                foreach ($relationType as $relation) {
                    if ($project["id"] == $relation["project_id"]) {
                        array_push($project[$key],
                            $this->_findRelation($projectsMeta[$key], $relation[$relationKeys[$key]]));
                    }
                }
            }
        }

        return $projects;
    }

    private function _findRelation($relations, $id)
    {
        foreach ($relations as $relation) if ($relation["id"] == $id) return $relation;
        return [];
    }

    public function getProjectAction($id)
    {
        $selectedProject = isset($id) ? $id : $this->request->id;
        $projects = $this->getProjectsAction();
        foreach ($projects as $project) if ($project["id"] == $selectedProject) return $project;
        return [];
    }
}
