<?php

class BaseController
{
    protected $dbConnection;
    protected $request;

    public function __construct()
    {
        $this->dbConnection = new DatabaseConnection();
        $this->parseBody();
    }

    public function callAction()
    {
        $this->_callAction($this->request->action);
    }

    /**
     * Calls user specified action if present
     * @param $action
     */
    protected function _callAction($action)
    {
        if (!$action) $this->JSONResponse(["error" => "You must specify action"]);
        if (!method_exists($this, $action . "Action")) $this->JSONResponse(["error" => "There is no such action"]);
        $response = call_user_func(array($this, $action . "Action"));
        $this->JSONResponse($response);
    }

    /**
     * Makes JSON response and ends script
     * @param $response
     */
    protected function JSONResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
        exit();
    }

    /**
     * Parses JSON body and assigns it to object instance
     */
    public function parseBody()
    {
        $plainRequest = file_get_contents('php://input');
        $this->request = json_decode($plainRequest);
    }
}
