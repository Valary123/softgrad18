<?php

session_start();

spl_autoload_register(function ($className) {
    $className = str_replace("\\", "/", $className);
    include "./classes/$className.php";
});

use Dotenv\Dotenv;
$dotenv = new Dotenv(__DIR__ . "/../..");
$dotenv->load();

require "classes/DatabaseConnection.php";
require "controllers/BaseController.php";

