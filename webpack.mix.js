const mix = require('laravel-mix');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

mix.setPublicPath('dist/public')
  .webpackConfig(() => ({
    plugins: [
      new Dotenv(),
      new HtmlWebpackPlugin({
        inject: false,
        filename: '../index.html',
        template: './src/index.ejs',
        version: +new Date(),
      }),
    ],
  }))
  .autoload({
    jquery: [
      '$', 'jQuery', 'window.jQuery', 'window.$',
    ],
  })
  .js('src/js/vendor.js', 'dist/public/js')
  .js('src/js/app.js', 'dist/public/js')
  .js('src/js/admin.js', 'dist/public/js')
  .styles([
    'src/css/bootstrap.min.css',
    'node_modules/vue-multiselect/dist/vue-multiselect.min.css',
    'src/css/font-awesome.min.css',
    'src/css/fonts.css',
    'src/css/et-line.css',
    'src/css/dinweb.css',
    'src/css/owl.carousel.min.css',
    'src/css/owl.theme.default.min.css',
    'src/css/responsive.css',
    'src/css/main.css',
    'src/css/style.css',
    'src/css/projects.css',
    'src/css/vacancies.css',
    'src/css/team-gallery.css',
    'src/css/style-5.css',
    'src/css/devicon.min.css',
    'node_modules/owl.carousel/dist/assets/owl.carousel.css',
    'node_modules/slick-carousel/slick/slick.css',
  ], 'dist/public/css/app.css')
  .copy('src/favicon.ico', 'dist')
  // .copy('src/index.html', 'dist/index.html')
  .copy('src/admin.html', 'dist/admin.html')
  .copy('src/fonts', 'dist/public/fonts')
  .copy('public/img', 'dist/public/img');
