const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const Prerenderer = require('@prerenderer/prerenderer');
const RendererPuppeteer = require('@prerenderer/renderer-puppeteer');

const prerenderer = new Prerenderer({
  staticDir: path.join(__dirname, 'dist'),
  renderer: new RendererPuppeteer({
    renderAfterTime: 3000,
    args: ['--disable-web-security', '--disable-background-timer-throttling'],
    defaultViewport: {
      width: 1280,
      height: 1024,
    },
  }),
});


prerenderer.initialize()
  .then(() => prerenderer.renderRoutes(['/', '/vacancies', '/projects']))
  .then(renderedRoutes => {
    renderedRoutes.forEach(renderedRoute => {
      try {
        const outputDir = path.join(__dirname, 'dist');
        let outputFile = '';

        if (renderedRoute.route === '/') {
          outputFile = `${outputDir}/index.html`;
        } else {
          outputFile = `${outputDir}/${renderedRoute.route}.html`;
        }

        mkdirp.sync(outputDir);
        fs.writeFileSync(outputFile, renderedRoute.html.trim());
      } catch (e) {
        // Handle errors.
      }
    });

    fs.copyFileSync('./src/.prerender-htaccess', './dist/.htaccess');
    prerenderer.destroy();
  })
  .catch(() => {
    prerenderer.destroy();
  });
