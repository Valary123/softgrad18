const { ncp } = require('ncp');
const mkdirp = require('mkdirp');

mkdirp('dist');
mkdirp('dist/public');

const $baseAssets = ['robots.txt', '.htaccess', 'backend'];
$baseAssets.forEach(asset => ncp(`src/${asset}`, `dist/${asset}`));

const $folders = ['video', 'photos', 'projects', 'svg'];
$folders.forEach(folder => ncp(`public/${folder}`, `dist/public/${folder}`));


ncp('src/js/vendor/flowplayer', 'dist/flowplayer');
ncp('src/js/vendor/tinymce', 'dist/tinymce');
